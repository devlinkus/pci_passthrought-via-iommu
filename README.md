# PCI passthrough via OVMF #

### Links ###
* [PCI passthrough via OVMF](https://wiki.archlinux.org/index.php/PCI_passthrough_via_OVMF)
* [nvidia-kvm-patcher](https://github.com/sk1080/nvidia-kvm-patcher)
* [Installing Drivers during the Windows Installation](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/virtualization_host_configuration_and_guest_installation_guide/form-virtualization_host_configuration_and_guest_installation_guide-para_virtualized_drivers-installing_with_a_virtualized_floppy_disk)
* [https://passthroughpo.st/](https://passthroughpo.st/)


### Prerequisites ###

* Hardware
	* CPU support for IOMMU
	* motherboard support for IOMMU
	* 2 GPU (ex: GPU + i GPU)
	* Keyboard**S** & Mouse**s**
	* guest GPU ROM must support UEFI.
* Bios
	* Enable **Intel VT-x** and **Intel VT-d**
	* Enable iGPU (host) as main dislay
	
	

### Enabling IOMMU ###

Update `/etc/default/grub` by creating/updating this line:
```
#!bash
GRUB_CMDLINE_LINUX_DEFAULT="quiet iommu=pt intel_iommu=on"
```

Regenerate grub:
```
#!bash
grub-mkconfig -o /boot/grub/grub.cfg
```

After rebooting, check dmesg to confirm that IOMMU has been correctly enabled: 

```
#!bash
dmesg | grep -e DMAR -e IOMMU
```

```
#!bash
...
[    0.000000] DMAR: IOMMU enabled
[    0.559348] DMAR: Intel(R) Virtualization Technology for Directed I/O
...
```


### Ensuring that the groups are valid ###

Use [iommu_groups.sh](https://bitbucket.org/devlinkus/pci_passthrought-via-iommu/src/master/iommu_groups.sh)
to check mapped to IOMMU groups of your PCI devices

Ex:
```
#!bash
IOMMU Group 0 00:00.0 Host bridge [0600]: Intel Corporation Intel Kaby Lake Host Bridge [8086:591f] (rev 05)
IOMMU Group 1 00:01.0 PCI bridge [0604]: Intel Corporation Xeon E3-1200 v5/E3-1500 v5/6th Gen Core Processor PCIe Controller (x16) [8086:1901] (rev 05)
IOMMU Group 1 01:00.0 VGA compatible controller [0300]: NVIDIA Corporation GP104 [GeForce GTX 1080] [10de:1b80] (rev a1)
IOMMU Group 1 01:00.1 Audio device [0403]: NVIDIA Corporation GP104 High Definition Audio Controller [10de:10f0] (rev a1)
IOMMU Group 10 00:1f.6 Ethernet controller [0200]: Intel Corporation Ethernet Connection (2) I219-V [8086:15b8]
IOMMU Group 11 03:00.0 USB controller [0c03]: ASMedia Technology Inc. Device [1b21:2142]
IOMMU Group 2 00:02.0 VGA compatible controller [0300]: Intel Corporation HD Graphics 630 [8086:5912] (rev 04)
IOMMU Group 3 00:08.0 System peripheral [0880]: Intel Corporation Xeon E3-1200 v5/v6 / E3-1500 v5 / 6th/7th Gen Core Processor Gaussian Mixture Model [8086:1911]
IOMMU Group 4 00:14.0 USB controller [0c03]: Intel Corporation 200 Series PCH USB 3.0 xHCI Controller [8086:a2af]
```

On `Group 1`, we have our guest GPU

### Isolating the GPU ###

**Starting with Linux 4.18.16, vfio-pci is compiled-in as opposed to being a module.**

Pass all `Group 1` ids minus PCI bridge by updating `/etc/default/grub`:
```
#!bash
GRUB_CMDLINE_LINUX_DEFAULT="quiet iommu=pt intel_iommu=on vfio-pci.ids=10de:1b80,10de:10f0"
```

Regenerate grub:
```
#!bash
grub-mkconfig -o /boot/grub/grub.cfg
```

Reboot and check:
```
#!bash
dmesg | grep -i vfio
```
```
#!bash
[    0.000000] Command line: BOOT_IMAGE=/boot/vmlinuz-linux root=UUID=2d7165d6-b977-4beb-8d48-d738c794b4fa rw quiet iommu=pt intel_iommu=on vfio-pci.ids=10de:1b80,10de:10f0
[    0.000000] Kernel command line: BOOT_IMAGE=/boot/vmlinuz-linux root=UUID=2d7165d6-b977-4beb-8d48-d738c794b4fa rw quiet iommu=pt intel_iommu=on vfio-pci.ids=10de:1b80,10de:10f0
[    0.940307] VFIO - User Level meta-driver version: 0.3
[    0.940388] vfio-pci 0000:01:00.0: vgaarb: changed VGA decodes: olddecodes=io+mem,decodes=io+mem:owns=none
[    0.956789] vfio_pci: add [10de:1b80[ffff:ffff]] class 0x000000/00000000
[    0.973460] vfio_pci: add [10de:10f0[ffff:ffff]] class 0x000000/00000000
[    2.660359] vfio-pci 0000:01:00.0: vgaarb: changed VGA decodes: olddecodes=io+mem,decodes=io+mem:owns=none
```

check NVIDIA Corporation GP104 [GeForce GTX 1080] [10de:1b80]
```
#!bash
lspci -nnk -d 10de:1b80
```

```
#!bash
01:00.0 VGA compatible controller [0300]: NVIDIA Corporation GP104 [GeForce GTX 1080] [10de:1b80] (rev a1)
	Subsystem: eVga.com. Corp. GP104 [GeForce GTX 1080] [3842:6686]
	Kernel driver in use: vfio-pci
	Kernel modules: nouveau, nvidia_drm, nvidia
```

check NVIDIA Corporation GP104 High Definition Audio Controller [10de:10f0]
```
#!bash
lspci -nnk -d 10de:10f0
```

```
#!bash
01:00.1 Audio device [0403]: NVIDIA Corporation GP104 High Definition Audio Controller [10de:10f0] (rev a1)
	Subsystem: eVga.com. Corp. GP104 High Definition Audio Controller [3842:6686]
	Kernel driver in use: vfio-pci
	Kernel modules: snd_hda_intel

```

### Setting up an OVMF-based guest VM ###

Install `qemu`, `qemu-block-iscsi`, `libvirt`, `ovmf`, and `virt-manager`

Edit: `/etc/libvirt/qemu.conf`:

```
!#bash
nvram = [
	"/usr/share/ovmf/x64/OVMF_CODE.fd:/usr/share/ovmf/x64/OVMF_VARS.fd"
]
```

#### Setting up the guest OS ####

using `virt-manager`, you have to add your user to the `libvirt` user group to ensure authentication. 

* When the VM creation wizard asks you to name your VM (final step before clicking "Finish"), check the "Customize before install" checkbox.
* "Overview" section
	* firmeware to UEFI
* "CPU" section
	*  change your CPU model to "host-passthrough" (write it)
* VirtIO:
	* Install `virtio-win` (pacman)
	* go into "Add Hardware" and add a Controller for SCSI drives of the "VirtIO SCSI"
	* change the default IDE disk for VirtIO disk
	* go into "Add Hardware" and add an IDE CD-ROM with the virtio-win iso image (/usr/share/virtio/virtio-win.iso)
	* Check boot process: 1 VirtIO Disk + 2 CD-ROM (virtio-win: 1st, windows iso: 2nd)
	* Launch installation, then add drivers from virtio-win disk (vioscsi/w10/amd64)
	
	
Turn off the vm, remove the spice channel and virtual display, the QXL video adapter, the emulated mouse and keyboard and the USB tablet device.
Bind a few USB host devices to your VM: mouse keyboard

Attach the PCI device that was isolated earlier (Gpu + High Definition Audio Controller)
* "Add Hardware"
* select the PCI Host Devices you want to passthrough

### Tweak ###

You can edit your configuration with:

```
!#bash
sudo EDITOR=nano virsh edit win10
```

#### Nvidia driver ####
with recent lbvirt + qemu

add this to vm config:
```
<domain>
    ...
        <features>
            ...
            <kvm>
                <hidden state='on'/>
            </kvm>
            ...
            <hyperv>
                ...
                <vendor_id state='on' value='123456789ab'/>
            </hyperv>
            ...
        </features>
    ...
</domain>
```

### Same keybboard/mouse for host and guest

Check avaible device:

```
#!bash
ls -1 /dev/input/by-id/
usb-Microsoft_Microsoft®_Nano_Transceiver_v2.1-event-if01
usb-Microsoft_Microsoft®_Nano_Transceiver_v2.1-event-if02
usb-Microsoft_Microsoft®_Nano_Transceiver_v2.1-event-kbd
usb-Microsoft_Microsoft®_Nano_Transceiver_v2.1-if01-event-mouse
usb-Microsoft_Microsoft®_Nano_Transceiver_v2.1-if01-mouse
usb-Microsoft_Microsoft®_Nano_Transceiver_v2.1-if02-event-kbd
usb-SONiX_Evoluent_VerticalMouse_C-event-mouse
usb-SONiX_Evoluent_VerticalMouse_C-mouse
```

Check current device: `cat /dev/input/by-id/usb-Microsoft_Microsoft®_Nano_Transceiver_v2.1-event-if01`

Edit `/etc/libvirt/qemu.conf`:

replace 
```
#!bash
# cgroup_device_acl = [
#    "/dev/null", "/dev/full", "/dev/zero",
#    "/dev/random", "/dev/urandom",
#    "/dev/ptmx", "/dev/kvm", "/dev/kqemu",
#    "/dev/rtc","/dev/hpet", "/dev/sev",
#]
```
by
```
#!bash
cgroup_device_acl = [
    "/dev/null", "/dev/full", "/dev/zero",
    "/dev/random", "/dev/urandom",
    "/dev/ptmx", "/dev/kvm", "/dev/kqemu",
    "/dev/rtc","/dev/hpet", "/dev/sev",
    "/dev/input/by-id/usb-Microsoft_Microsoft®_Nano_Transceiver_v2.1-event-kbd",
    "/dev/input/by-id/usb-SONiX_Evoluent_VerticalMouse_C-event-mouse"
]
```
added device:
 - /dev/input/by-id/usb-Microsoft_Microsoft®_Nano_Transceiver_v2.1-event-kbd
 - /dev/input/by-id/usb-SONiX_Evoluent_VerticalMouse_C-event-mouse
 
 update guest config:
```
!#bash
sudo EDITOR=nano virsh edit win10
```

```
<qemu:commandline>
    <qemu:arg value='-object'/>
    <qemu:arg value='input-linux,id=mouse1,evdev=/dev/input/by-id/usb-SONiX_Evoluent_VerticalMouse_C-event-mouse'/>
    <qemu:arg value='-object'/>
    <qemu:arg value='input-linux,id=kbd1,evdev=/dev/input/by-id/usb-Microsoft_Microsoft®_Nano_Transceiver_v2.1-event-kbd,grab_all=on,repeat=on'/>
</qemu:commandline>
```







	
